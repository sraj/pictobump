# -*- coding: utf-8 -*-
from flask import current_app, g, session, redirect, url_for, request, flash, abort
from functools import wraps
from models.users import Users

class Auth(object):

    def __init__(self, app=None):
        self.skip_static = True
        self.login_endpoint = "auth.signin"
        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        app.auth = self
        app.before_request(self._before_request)
        app.after_request(self._after_request)

    def reload_auth(self):
        authid = session.get("emailid", None)
        if authid is None:
            g.auth = None
        else:
            user = Users.getUserByEmailId(emailid=authid)
            if user is None:
                logout_auth()
            else:
                g.auth = user
    
    def unauthorized(self):
        if not self.login_endpoint:
            abort(401)
        flash(u"Please log in to access this page.")
        return redirect(url_for(self.login_endpoint))

    def _before_request(self):
        if self._is_static_route():
            g.auth = None
            return
        self.reload_auth()

    def _after_request(self, response):
        if request.endpoint != 'static':
            response.headers["Cache-Control"] = "no-cache, no-store, max-age=0, must-revalidate"
            response.headers["Pragma"] = "no-cache"
            response.headers["Expires"] = 0
        return response

    def _is_static_route(self):
        return (self.skip_static and (request.endpoint == 'static'))

def login_required(fn):
    @wraps(fn)
    def decorated_view(*args, **kwargs):
        if not g.auth:
            return current_app.auth.unauthorized()
        return fn(*args, **kwargs)
    return decorated_view

def logout_auth():
    if "emailid" in session:
        del session['emailid']
    current_app.auth.reload_auth()
    return True