Welcome to Pictobump
--------------------

Pictobump game is played with friends trying to identify specific words 
from their buddys' drawings.

To Install
----------
pip install -r requirements.txt
npm install socket.io


To Start
--------
1.  navigate to pictobump and 
		python manage.py runserver (runs on 5000)

2. navigate to liveserver and 
		node server.js (runs on 4000)
